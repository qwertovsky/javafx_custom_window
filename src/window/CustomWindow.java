package window;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class CustomWindow extends Application
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		launch(args);
	}

	@Override
	public void start(Stage stage)
	{
		StackPane root = new StackPane();
		SVGPath p = new SVGPath();
		p.setContent("M0,0 l180,-150 a60,60 0 0,1 60,60 v180 a60,60 0 0,1 -60,60  h-180 a60,60 0 0,1 -60,-60 l0,-180  z");
		p.setFill(new Color(0.90,1,1,1));
		p.setEffect(new DropShadow());
		
		GridPane pane = null;

		try
		{
			pane = FXMLLoader.load(getClass().getResource("/resources/fxml_example.fxml"));
		} catch (IOException e)
		{
			e.printStackTrace();
			return;
		}
		Image image = new Image(getClass().getResourceAsStream("/resources/duke.png"));
		ImageView imageView = new ImageView(image);
		imageView.setScaleX(0.75);
		imageView.setScaleY(0.75);
		imageView.setTranslateY(-70);
		root.getChildren().add(p);
		root.getChildren().add(imageView);
		root.getChildren().add(pane);
		
		Scene scene = null;
		scene = new Scene(root, 400, 400, Color.TRANSPARENT);
		stage.setScene(scene);
		stage.initStyle(StageStyle.TRANSPARENT);
		stage.setTitle("Custom Window");
		stage.show();
		
	}
	
	

}
